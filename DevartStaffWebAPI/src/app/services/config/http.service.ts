import { Injectable } from '@angular/core';

@Injectable()
export class HttpConfigApi {

  constructor() { };
  host: string = 'http://localhost:8304/';
//  host: string = 'http://staffdevart.range.com.ua';
  teams: any = {
    get: this.host + '/api/teams/get/',
    add: this.host + '/api/teams/add/',
    update: this.host + '/api/teams/update/',
    delete: this.host + '/api/teams/delete/',
  };
  positions: any = {
    get: this.host + '/api/positions/get/',
    add: this.host + '/api/positions/add/',
    update: this.host + '/api/positions/update/',
    delete: this.host + '/api/positions/delete/',
  };
  staff: any = {
    get: this.host + '/api/staff/get/',
    add: this.host + '/api/staff/add/',
    update: this.host + '/api/staff/update/',
    delete: this.host + '/api/staff/delete/',
  };
}
