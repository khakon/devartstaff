import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import { Observable } from 'rxjs/Observable';
import { HttpConfigApi } from '../config/http.service';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import "rxjs/add/observable/throw";
import "rxjs/add/observable/from";

@Injectable()
export class TeamsService {

  constructor(private http: Http, private config: HttpConfigApi) { }
  getTeams(): Observable<any[]> {
    return this.http.get(this.config.teams.get)
      .map(response => response.json())
      .catch((error: any) => {
        return Observable.throw(error);
      });
  }
  getTeam(id: string): Observable<any[]> {
    return this.http.get(this.config.teams.get + id)
      .map(response => response.json())
      .catch((error: any) => {
        return Observable.throw(error);
      });
  }
  addTeam(team: any): Observable<any[]> {
    return this.http.post(this.config.teams.add, JSON.stringify(team))
      .map(response => response.json())
      .catch((error: any) => {
        return Observable.throw(error);
      });
  }
  updateTeam(team: any): Observable<any[]> {
    return this.http.post(this.config.teams.update, JSON.stringify(team))
      .map(response => response.json())
      .catch((error: any) => {
        return Observable.throw(error);
      });
  }
  deleteTeam(id: string): Observable<any[]> {
    return this.http.delete(this.config.teams.delete + id)
      .map(response => response.json())
      .catch((error: any) => {
        return Observable.throw(error);
      });
  }

}
