import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import { Observable } from 'rxjs/Observable';
import { HttpConfigApi } from '../config/http.service';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import "rxjs/add/observable/throw";
import "rxjs/add/observable/from";

@Injectable()
export class StaffService {

  constructor(private http: Http, private config: HttpConfigApi) { }
  getPersons(): Observable<any[]> {
    return this.http.get(this.config.staff.get)
      .map(response => response.json())
      .catch((error: any) => {
        return Observable.throw(error);
      });
  }
  getPerson(id: string): Observable<any[]> {
    return this.http.get(this.config.staff.get + id)
      .map(response => response.json())
      .catch((error: any) => {
        return Observable.throw(error);
      });
  }
  addPerson(person: any): Observable<any[]> {
    return this.http.post(this.config.staff.add, JSON.stringify(person))
      .map(response => response.json())
      .catch((error: any) => {
        return Observable.throw(error);
      });
  }
  updatePerson(person: any): Observable<any[]> {
    return this.http.post(this.config.staff.update, JSON.stringify(person))
      .map(response => response.json())
      .catch((error: any) => {
        return Observable.throw(error);
      });
  }
  deletePerson(id: string): Observable<any[]> {
    return this.http.delete(this.config.staff.delete + id)
      .map(response => response.json())
      .catch((error: any) => {
        return Observable.throw(error);
      });
  }

}
