import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import { Observable } from 'rxjs/Observable';
import { HttpConfigApi } from '../config/http.service';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import "rxjs/add/observable/throw";
import "rxjs/add/observable/from";

@Injectable()
export class PositionsService {

  constructor(private http: Http, private config: HttpConfigApi) { }
  getPositions(): Observable<any[]> {
    return this.http.get(this.config.positions.get)
      .map(response => response.json())
      .catch((error: any) => {
        return Observable.throw(error);
      });
  }
  getPosition(id: string): Observable<any[]> {
    return this.http.get(this.config.positions.get + id)
      .map(response => response.json())
      .catch((error: any) => {
        return Observable.throw(error);
      });
  }
  addPosition(position: any): Observable<any[]> {
    return this.http.post(this.config.positions.add, JSON.stringify(position))
      .map(response => response.json())
      .catch((error: any) => {
        return Observable.throw(error);
      });
  }
  updatePosition(position: any): Observable<any[]> {
    return this.http.post(this.config.positions.update, JSON.stringify(position))
      .map(response => response.json())
      .catch((error: any) => {
        return Observable.throw(error);
      });
  }
  deletePosition(id: string): Observable<any[]> {
    return this.http.delete(this.config.positions.delete + id)
      .map(response => response.json())
      .catch((error: any) => {
        return Observable.throw(error);
      });
  }

}
