import {Injectable} from '@angular/core';

export interface BadgeItem {
  type: string;
  value: string;
}

export interface ChildrenItems {
  state: string;
  target?: boolean;
  name: string;
  type?: string;
  children?: ChildrenItems[];
}

export interface MainMenuItems {
  state: string;
  short_label?: string;
  main_state?: string;
  target?: boolean;
  name: string;
  type: string;
  icon: string;
  badge?: BadgeItem[];
  children?: ChildrenItems[];
}

export interface Menu {
  label: string;
  main: MainMenuItems[];
}

const MENUITEMS = [
  {
    label: 'Сотрудники',
    main: [
      {
        state: 'references',
        short_label: 'R',
        name: 'Справочники',
        type: 'sub',
        icon: 'ti-view-grid',
        children: [
          {
            state: 'teams',
            name: 'Команды'
          },
          {
            state: 'positions',
            name: 'Должности'
          },
          {
            state: 'staff',
            name: 'Сотрудники'
          },

        ]
      }
    ],
  },
];

@Injectable()
export class MenuItems {
  getAll(): Menu[] {
    return MENUITEMS;
  }

  /*add(menu: Menu) {
    MENUITEMS.push(menu);
  }*/
}
