import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { PositionsComponent } from './positions.component';
import { SharedModule } from '../../../shared/shared.module';
import { DxDataGridModule, DxButtonModule, DxLoadPanelModule, DxPopupModule, DxSelectBoxModule, DxTextAreaModule, DxFormModule, DxSchedulerModule, DxTemplateModule, DxChartModule, } from 'devextreme-angular';

export const PositionsRoutes: Routes = [{
  path: '',
  component: PositionsComponent,
  data: {
    breadcrumb: 'Должности',
    icon: 'icofont icofont-file-document bg-c-pink'
  }
}];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(PositionsRoutes),
    SharedModule,
    DxButtonModule,
    DxDataGridModule,
    DxLoadPanelModule,
    DxPopupModule,
    DxSelectBoxModule,
    DxTextAreaModule,
    DxFormModule,
    DxTemplateModule,
    DxSchedulerModule,
    DxChartModule,
  ],
  declarations: [PositionsComponent]
})
export class PositionsModule { }
