import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReferencesComponent } from './references.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

export const ReferencesRoutes: Routes = [
  {
    path: '',
    component: ReferencesComponent,
    children: [
      {
        path: 'default',
        loadChildren: './staff/staff.module#StaffModule'
      },
      {
        path: 'teams',
        loadChildren: './teams/teams.module#TeamsModule'
      },
      {
        path: 'staff',
        loadChildren: './staff/staff.module#StaffModule'
      },
      {
        path: 'positions',
        loadChildren: './positions/positions.module#PositionsModule'
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ReferencesRoutes),
    SharedModule
  ],
  declarations: [ReferencesComponent]
})
export class ReferencesModule { }
