import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-references',
  template: '<router-outlet><app-spinner></app-spinner></router-outlet>'
})
export class ReferencesComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
