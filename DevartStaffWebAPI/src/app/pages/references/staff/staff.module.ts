import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { StaffComponent } from './staff.component';
import { SharedModule } from '../../../shared/shared.module';
import { DxDataGridModule, DxButtonModule, DxLoadPanelModule, DxPopupModule, DxSelectBoxModule, DxTextAreaModule, DxFormModule, DxSchedulerModule, DxTemplateModule, DxChartModule, } from 'devextreme-angular';

export const StaffRoutes: Routes = [{
  path: '',
  component: StaffComponent,
  data: {
    breadcrumb: 'Сотрудники',
    icon: 'icofont icofont-file-document bg-c-pink'
  }
}];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(StaffRoutes),
    SharedModule,
    DxButtonModule,
    DxDataGridModule,
    DxLoadPanelModule,
    DxPopupModule,
    DxSelectBoxModule,
    DxTextAreaModule,
    DxFormModule,
    DxTemplateModule,
    DxSchedulerModule,
    DxChartModule,
  ],
  declarations: [StaffComponent]
})
export class StaffModule { }
