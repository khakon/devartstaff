import { Component, OnInit } from '@angular/core';
import { Person } from "./person";
import { StaffService } from '../../../services/http/staff.service';

@Component({
  selector: 'app-staff',
  templateUrl: './staff.component.html',
  styleUrls: ['./staff.component.css']
})
export class StaffComponent implements OnInit {

  persons: any[] = [];
  error: any;
  person: Person;
  popupVisibleAdd: boolean;
  popupVisibleDelete: boolean;
  loadingVisible: boolean;
  constructor(private dataSource: StaffService) { }

  selectHandler = {
    onSelectionPersonChanged: this.selectPerson.bind(this),
  }
  ngOnInit() {
  }

  onRowPrepared(e): void { };
  onToolbarPreparing(e): void {
    e.toolbarOptions.items.unshift({
      location: 'before',
      widget: 'dxButton',
      options: {
        width: 136,
        text: 'Добавить',
        onClick: this.addPerson.bind(this)
      }
    })
  };
  selectPerson(e): void { };
  addPerson(e): void { };
  editActionOptions = {
    onClick: this.edit.bind(this)//() => notify("The edit button was clicked")
  };
  deleteActionOptions = {
    onClick: this.delete.bind(this)//() => notify("The delete button was clicked")
  };
  viewActionOptions = {
    onClick: this.view.bind(this)//() => notify("The view button was clicked")
  };
  add(): void {
    this.person = new Person(null);
    this.popupVisibleAdd = true;
  }
  edit(data: any) {
    this.loadingVisible = true;
    this.dataSource.getPerson(data.id).subscribe(
      data => {
        this.person = new Person(data);
        this.loadingVisible = false;
        this.popupVisibleAdd = true;
      },
      error => {
        this.error = error;
        console.log(error);
        this.loadingVisible = false;
      }
    );
  };
  delete(data: any) {
    this.dataSource.getPerson(data.id).subscribe(
      data => {
        this.person = data;
        this.loadingVisible = false;
        this.popupVisibleDelete = true;
      },
      error => {
        this.error = error;
        console.log(error);
        this.loadingVisible = false;
      }
    );
  };
  view(data: any) { console.log(data); };
}
