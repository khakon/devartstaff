import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-teams',
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.css']
})
export class TeamsComponent implements OnInit {

  team: any[] = [];

  constructor() { }

  selectHandler = {
    onSelectionTeamChanged: this.selectTeam.bind(this),
  }
  ngOnInit() {
  }

  onRowPrepared(e): void { };
  onToolbarPreparing(e): void {
    e.toolbarOptions.items.unshift({
      location: 'before',
      widget: 'dxButton',
      options: {
        width: 136,
        text: 'Добавить',
        onClick: this.addTeam.bind(this)
      }
    })
  };
  selectTeam(e): void { };
  addTeam(e): void { };

}
