import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { TeamsComponent } from './teams.component';
import { SharedModule } from '../../../shared/shared.module';
import { DxDataGridModule, DxButtonModule, DxLoadPanelModule, DxPopupModule, DxSelectBoxModule, DxTextAreaModule, DxFormModule, DxSchedulerModule, DxTemplateModule, DxChartModule, } from 'devextreme-angular';

export const TeamsRoutes: Routes = [{
  path: '',
  component: TeamsComponent,
  data: {
    breadcrumb: 'Команды',
    icon: 'icofont icofont-file-document bg-c-pink'
  }
}];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(TeamsRoutes),
    SharedModule,
    DxButtonModule,
    DxDataGridModule,
    DxLoadPanelModule,
    DxPopupModule,
    DxSelectBoxModule,
    DxTextAreaModule,
    DxFormModule,
    DxTemplateModule,
    DxSchedulerModule,
    DxChartModule,
  ],
  declarations: [TeamsComponent]
})
export class TeamsModule { }
