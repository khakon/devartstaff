using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DevartStaffRepository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DevartStaffWebAPI.Controllers
{
  [Produces("application/json")]
  public class TeamController : Controller
  {

    private StaffDbContext _db;

    public TeamController(StaffDbContext db)
    {
      _db = db;
    }

    [HttpGet]
    [Route("api/teams")]
    public IActionResult Teams()
    {
      try
      {
        var model = _db.Teams.ToList();
        return Ok(model);
      }
      catch (Exception ex)
      {
        return StatusCode(500);
      }
    }

    [HttpGet]
    [Route("api/teams/{id}")]
    public IActionResult Team(int id)
    {
      try
      {
        var model = _db.Teams.FirstOrDefault(t => t.TeamId == id);
        return Ok(model);
      }
      catch (Exception ex)
      {
        return StatusCode(500);
      }
    }

    [HttpPost]
    [Route("api/teams")]
    public IActionResult Add([FromBody]Team model)
    {
      try
      {
        if (_db.Teams.Any(s => s.TeamId == model.TeamId))
        {
          var item = _db.Teams.SingleOrDefault(s => s.TeamId == model.TeamId);
          item.Title = model.Title;
          item.Description = model.Description;
          item.UserId = 0;
          item.DateChange = DateTime.Now;
          _db.Update(item);
          _db.SaveChanges();
        }
      }
      catch (Exception ex)
      {
        return BadRequest(ex.Message);
      }
      return RedirectToAction("Teams");
    }

    [HttpDelete]
    [Route("api/teams/delete/{id}")]
    public IActionResult Delete(int id)
    {
      try
      {
        if (!_db.Teams.Any(s => s.TeamId == id)) return BadRequest("Команда не найдена");//NotFound
        var item = _db.Teams.FirstOrDefault(s => s.TeamId == id);
        _db.Remove(item);
        _db.SaveChanges();
        return RedirectToAction("Teams");
      }
      catch (Exception ex)
      {
        return BadRequest(ex.Message);
      }
    }
  }
}
