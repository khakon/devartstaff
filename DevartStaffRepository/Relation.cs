﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevartStaffRepository
{
    public class Relation :Item
    {
        public int RelationId { get; set; }
        public int PersonId { get; set; }
        public int RelationTypeId { get; set; }
        public string Name { get; set; }
        public DateTime BirthDate { get; set; }
    }
}
