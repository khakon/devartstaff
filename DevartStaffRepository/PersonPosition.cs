﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevartStaffRepository
{
    class PersonPosition:Item
    {
        public int Id { get; set; }
        public int PositionId { get; set; }
        public int PersonId { get; set; }

        public Person Person { get; set; }
        public Position Position { get; set; }
    }
}
