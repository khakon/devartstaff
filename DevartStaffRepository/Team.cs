﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevartStaffRepository
{
    public class Team :Item
    {
        public int TeamId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }
}
