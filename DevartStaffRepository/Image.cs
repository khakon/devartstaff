﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevartStaffRepository
{
    public class Image:Item
    {
        public int ImageId { get; set; }
        public int PersonId { get; set; }
        public byte[] Data { get; set; }
        public string Path { get; set; }
        public bool Main { get; set; }
    }
}
