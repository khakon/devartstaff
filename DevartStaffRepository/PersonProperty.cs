﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevartStaffRepository
{
    public class PersonProperty :Item
    {
        public int PersonPropertyId { get; set; }
        public int PersonId { get; set; }
        public int PropertyTypeId { get; set; }
        public string Value { get; set; }
    }
}
