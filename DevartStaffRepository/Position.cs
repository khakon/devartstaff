﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevartStaffRepository
{
    public class Position :Item
    {
        public int PositionId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }
}
