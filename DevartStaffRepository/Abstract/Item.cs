﻿using System;

namespace DevartStaffRepository
{
    public abstract class Item
    {
        public int UserId { get; set; }
        public bool Delete { get; set; }
        public DateTime DateCreate { get; set; }
        public DateTime DateChange { get; set; }
    }
}
