﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevartStaffRepository
{
    public class Person :Item
    {
        public int PersonId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string ShortName { get; set; }
        public DateTime HireDate { get; set; }
        public DateTime QuitDate { get; set; }
        public DateTime BirthDate { get; set; }
    }
}
