﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevartStaffRepository
{
    class PersonTeam:Item
    {
        public int Id { get; set; }
        public int TeamId { get; set; }
        public int PersonId { get; set; }

        public Person Person { get; set; }
        public Team Team { get; set; }
    }
}
