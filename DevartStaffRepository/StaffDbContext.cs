﻿using Microsoft.EntityFrameworkCore;

namespace DevartStaffRepository
{
    public class StaffDbContext : DbContext
    {
        public virtual DbSet<Image> Images { get; set; }
        public virtual DbSet<Person> Persons { get; set; }
        public virtual DbSet<PersonProperty> PersonProperties { get; set; }
        public virtual DbSet<PropertyType> PropertyTypes { get; set; }
        public virtual DbSet<Position> Positions { get; set; }
        public virtual DbSet<Relation> Relations { get; set; }
        public virtual DbSet<RelationType> RelationTypes { get; set; }
        public virtual DbSet<Team> Teams { get; set; }
        public virtual DbSet<User> Users { get; set; }

        public StaffDbContext(DbContextOptions<StaffDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
